provider "aws"{
   access_key="AKIARHQX6XPRGTA5GUPU"
   secret_key="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"
    region="us-east-1"
}
terraform {
    required_version=">=v1.1.0"
}

resource "aws_vpc" "cloudbinary_vpc"{
    cidr_block="192.168.0.0/16"
    instance_tenancy="default"
    enable_dns_support="true"
    enable_dns_hostnames="true"

    tags={
        Name="cloudbinary_vpc"
        CreatedBy="IAC-Terraform"
    }
}


resource "aws_route_table" "cloudbinary_rtb_private"{
  vpc_id=aws_vpc.cloudbinary_vpc.id

  tags={
      Name="cloudninary_rtb_private"
      CreatedBy="IAC-Terraform"
  }  
}
resource "aws_route_table" "cloudbinary_rtb_public"{
  vpc_id=aws_vpc.cloudbinary_vpc.id

  tags={
      Name="cloudninary_rtb_public"
      CreatedBy="IAC-Terraform"
  }  
}

resource "aws_subnet" "cloudbinary_subnet_public1"{
    vpc_id=aws_vpc.cloudbinary_vpc.id
    cidr_block="192.168.1.0/24"
    map_public_ip_on_launch=true
    availability_zone="us-east-1a"

    tags={
        Name="cloudbinary_subnet_public1"
        CreatedBy="IAc-Terraform"

    }

}
resource "aws_subnet" "cloudbinary_subnet_public2"{
    vpc_id=aws_vpc.cloudbinary_vpc.id
    cidr_block="192.168.2.0/24"
    map_public_ip_on_launch=true
    availability_zone="us-east-1b"

    tags={
        Name="cloudbinary_subnet_public2"
        CreatedBy="IAc-Terraform"

    }

}
resource "aws_subnet" "cloudbinary_subnet_private1"{
    vpc_id=aws_vpc.cloudbinary_vpc.id
    cidr_block="192.168.3.0/24"
      availability_zone="us-east-1a"

    tags={
        Name="cloudbinary_subnet_private1"
        CreatedBy="IAc-Terraform"

    }

}
resource "aws_subnet" "cloudbinary_subnet_private2"{
    vpc_id=aws_vpc.cloudbinary_vpc.id
    cidr_block="192.168.4.0/24"
      availability_zone="us-east-1b"

    tags={
        Name="cloudbinary_subnet_private2"
        CreatedBy="IAc-Terraform"

    }

}
resource "aws_route_table_association" "cloudbinary_add_sub_pub1_rtb"{
subnet_id=aws_subnet.cloudbinary_subnet_public1.id
route_table_id=aws_route_table.cloudbinary_rtb_public.id


}
resource "aws_route_table_association" "cloudbinary_add_sub_pub2_rtb"{
subnet_id=aws_subnet.cloudbinary_subnet_public2.id
route_table_id=aws_route_table.cloudbinary_rtb_public.id


}
resource "aws_route_table_association" "cloudbinary_add_sub_pri1_rtb"{
subnet_id=aws_subnet.cloudbinary_subnet_private1.id
route_table_id=aws_route_table.cloudbinary_rtb_private.id


}
resource "aws_route_table_association" "cloudbinary_add_sub_pri2_rtb"{
subnet_id=aws_subnet.cloudbinary_subnet_private2.id
route_table_id=aws_route_table.cloudbinary_rtb_private.id


}
resource "aws_internet_gateway" "cloudbinary_igw"{
vpc_id=aws_vpc.cloudbinary_vpc.id
tags={
    Name="cloudbinary_igw"
    CreatedBy="IAC-Terraform"
}
}
 resource "aws_route" "cloudbinary_rtb_igw"{
     route_table_id=aws_route_table.cloudbinary_rtb_public.id
     gateway_id=aws_internet_gateway.cloudbinary_igw.id
     destination_cidr_block="0.0.0.0/0"

 }

 resource "aws_eip""cloudbinary_eip"{
     vpc=true
 }

 resource "aws_nat_gateway" "cloudbinary_natgateway"{
subnet_id=aws_subnet.cloudbinary_subnet_public1.id
allocation_id=aws_eip.cloudbinary_eip.allocation_id
 }

 resource "aws_route" "cloudbinary_rtb_natgateway"{
     route_table_id=aws_route_table.cloudbinary_rtb_private.id
     gateway_id=aws_nat_gateway.cloudbinary_natgateway.id
     destination_cidr_block="0.0.0.0/0"
 }

 resource "aws_security_group" "cloudbinary_sg_bastion"{
     vpc_id= aws_vpc.cloudbinary_vpc.id
     name ="sg_bastion"
     description="Bastion SSH or RDP"

     ingress{
         description="SSH"
         cidr_blocks= ["0.0.0.0/0"]
         from_port=22
         to_port=22
         protocol="tcp"
     }
      ingress{
         description="RDP"
         cidr_blocks= ["0.0.0.0/0"]
         from_port=3389
         to_port=3389
         protocol="tcp"
     }

      egress{
         description="All"
         cidr_blocks= ["0.0.0.0/0"]
         from_port=0
         to_port=0
         protocol="-1"
     }

     tags={
         name="sg_bastion"
         CreatedBy="IAC"
     }

 }
 resource "aws_security_group" "cloudbinary_sg_dmz"{
    vpc_id=aws_vpc.cloudbinary_vpc.id
    description="DMZ - SSH - RDP - HTTP - HTTPS - DB"
    name="sg_dmz"

    ingress{
        description = "SSH"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 22
      to_port = 22
      protocol = "tcp"
    }
    ingress{
        description = "RDP"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 3389
      to_port = 3389
      protocol = "tcp"
    }
    ingress{
        description = "HTTP"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 80
      to_port = 80
      protocol = "tcp"
    }

    ingress{
        description = "HTTPS"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 443
      to_port = 443
      protocol = "tcp"
    }

    ingress{
        description = "DB"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 3306
      to_port = 3306
      protocol = "tcp"
    }

    egress{
        description = "All"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 0
      to_port = 0
      protocol = "-1"
    }

    tags={
        name="sg-dmz"
        CreatedBy="IAC"
    }
}
# Bastion
resource "aws_instance" "cloudbinary_bastion" {
    ami = "ami-04505e74c0741db8d "
    instance_type = "t2.micro"
    key_name = "Paurnami"
    subnet_id = aws_subnet.cloudbinary_subnet_public1.id  
    vpc_security_group_ids = [aws_security_group.cloudbinary_sg_bastion.id]

    tags = {
        Name = "BASTION"
        CreatedBy = "IAC"
    }
}

# WebServer
resource "aws_instance" "cloudbinary_web" {
    ami = "ami-04505e74c0741db8d"
    instance_type = "t2.micro"
    key_name = "Paurnami"
    subnet_id = aws_subnet.cloudbinary_subnet_private1.id    
    vpc_security_group_ids = [aws_security_group.cloudbinary_sg_dmz.id]

    tags = {
        Name = "WEBServer"
        CreatedBy = "IAC"
    }
}